﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    float hor;
    float ver;
    public float speed;
    //private Vector3 Direcao;
    private Rigidbody2D rb2d;
    void Start(){
        rb2d = GetComponent<Rigidbody2D>();
    }
	// Update is called once per frame
	void Update (){
        PlayerMove();
    }
    private void PlayerMove(){
        hor = Input.GetAxisRaw("Horizontal");
        ver = Input.GetAxisRaw("Vertical");

        rb2d.velocity = new Vector2(hor * speed, ver * speed);
        if (Input.GetAxis("Horizontal") > 0)
        {
            transform.eulerAngles = new Vector2(0, 0);
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            transform.eulerAngles = new Vector2(0, 180);
        }
    }
}
