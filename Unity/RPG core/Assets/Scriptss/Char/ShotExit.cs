﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotExit : MonoBehaviour
{
    public Transform ShotOrigin;
    public GameObject Tiro;
    private Vector2 LookDirection;
    private float LookAngle;
    public float Speed;
    public float Time;
    public bool CanShoot;
    private Level CurrentStat;
    public GameObject Player;
    void Start(){
        CanShoot = true;
        CurrentStat = Player.GetComponent<Level>();
    }
    // Update is called once per frame
    void Update(){
        LookAt();
        ShootBullet();
        StatsModifications();
    }

    void LookAt(){
        LookDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        LookAngle = Mathf.Atan2(LookDirection.y, LookDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, LookAngle - 90f);
    }

    void StatsModifications(){
        Time = 1 - (CurrentStat.Dex * 0.02f);
    }
    void ShootBullet(){
        if(CanShoot == true){
            if(Input.GetMouseButton(0)){
                GameObject bullet = Instantiate(Tiro, ShotOrigin.position, ShotOrigin.rotation);
                bullet.GetComponent<Rigidbody2D>().velocity = ShotOrigin.up * Speed;
                StartCoroutine(Delay());
            }
        }
    }
    IEnumerator Delay(){
        CanShoot = false;
        yield return new WaitForSeconds(Time);
        CanShoot = true;
    }
}
