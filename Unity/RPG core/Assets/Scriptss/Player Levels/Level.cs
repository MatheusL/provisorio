﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    [Header("Variaveis de nivel")]
    public int Experience;
    public int MaxExp;
    public int Levels;
    public int StatsPoint;
    [Header("Variaveis de atributos")]
    public int Luk;
    public int For;
    public int Dex;
    public int Vit;
    [Header("Variaveis de permisão")]
    public bool IsAdmin;

    // Start is called before the first frame update
    void Start()
    {
        StatsPoint = 0;
        Levels = 1;
    }
    // Update is called once per frame
    void Update()
    {
        ExpCalculo();
        AdminCommands();
    }

    void ExpCalculo(){
        MaxExp = Levels * 300;
        if(Experience >= MaxExp){
            Levels++;
            Experience = 0;
            StatsPoint += 1;
        }
    }
    void AdminCommands(){
        if(IsAdmin == true){
            if(Input.GetKeyDown(KeyCode.C) && StatsPoint > 0){
                Experience += 100;
                Dex+= 10;
                StatsPoint--;
            }
            if(Input.GetKeyDown(KeyCode.R)){
                Experience += 1000;
            }
        }
    }
    public void UpDex(){
        Dex += 10;
        StatsPoint--;
    }
}
