﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public bool enterCover; //entrou em contato com o cover
    public bool inCover; //esta protegido pelo cover
    public float speed;
    private float moveSpeed; //velocidade do movimento do player
    private Rigidbody2D rb2d;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        moveSpeed = speed;
        enterCover = false;
        inCover = false;
    }

    void Update()
    {
        Moviment();
        Cover();
    }

    //função que controla o movimento do player
    void Moviment()
    {
        if (Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f )
        {
            //transform.Translate (new Vector3(Input.GetAxisRaw("Horizontal") * moveSpeed * Time.deltaTime, 0f, 0f ));
            rb2d.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * moveSpeed, rb2d.velocity.y);
        }

        if (Input.GetAxisRaw("Vertical") > 0.5f || Input.GetAxisRaw("Vertical") < -0.5f )
        {
            //transform.Translate (new Vector3(0f, Input.GetAxisRaw("Vertical") * moveSpeed * Time.deltaTime, 0f));
            rb2d.velocity = new Vector2(rb2d.velocity.x , Input.GetAxisRaw("Vertical") * moveSpeed);
        }
        
        if (Input.GetAxisRaw("Vertical") < 0.5f && Input.GetAxisRaw("Vertical") > -0.5f )
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, 0f);
        }
        
        if (Input.GetAxisRaw("Horizontal") < 0.5f && Input.GetAxisRaw("Horizontal") > -0.5f )
        {
            rb2d.velocity = new Vector2(0f, rb2d.velocity.y);
        }
        
    }

    void Cover()
    {
        if(Input.GetKeyDown(KeyCode.E) && enterCover == true && inCover == false)
        {
            inCover = true;
            //fica imortal?
        }
        
        else if(Input.GetKeyDown(KeyCode.E) && enterCover == true && inCover == true)
        {
            inCover = false;
        }
        
    }

    void OnCollisionStay2D(Collision2D other)
    {
        if(other.gameObject.tag == "Cover")
        {
            enterCover = true;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if(other.gameObject.tag == "Cover")
        {
            inCover = false;
            enterCover = false;
        }
    }    
}