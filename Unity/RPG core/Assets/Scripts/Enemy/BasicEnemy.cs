﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemy : MonoBehaviour
{
    public int Health;
    public gameObject bullet;
    //private GameObject Player;
    //private Level DarExp; 
    //public int MobExp;
    
    void Start()
    {
        //Player = GameObject.FindWithTag("Player");
        //DarExp = Player.GetComponent<Level>();
    }

    
    void Update()
    {
        Morrer();
    }
    void Shoot(gameObject player){
        transform.LookAt(player);
        if(CanShoot == true){
            Instantiate(bullet);
            bullet.GetComponent<Rigidbody2D>().velocity = ShotOrigin.up * Speed;
            StartCoroutine(Delay());
        }
    }
    void OnTriggerEnter2D(Collider2D other){
        if(other.gameObject.tag == "Player"){
            Shoot(other);
        }

        /*
        if(other.gameObject.tag == "Projectile"){
            CausarDano();
            Destroy(other.gameObject);
        }
        */
    }
    void CausarDano(){   
            Health--;
    }
    void Morrer(){
        if(Health <= 0){
            //DarExp.Experience += MobExp;
            Destroy(gameObject);
        }
    }
}
